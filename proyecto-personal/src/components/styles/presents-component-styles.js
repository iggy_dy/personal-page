import styled from 'styled-components';

export const PresnetsComponentStyles = styled.div`
padding: 100px 150px 50px 150px;

.home-image{
    img{
        position: absolute;
        width:100%;
        clip-path: polygon(0 50%, 100% 20%, 100% 70%, 0% 100%);
        z-index: -1;
    }
 }

.presents-title{
    color: #000;
    font-size: 36px;
    font-weight: 800;
    opacity: 0.3;
    letter-spacing: 10px;
    text-align: center;
    margin-bottom: 100px;
}

.presents-container{
    display: flex;
    justify-content: space-between;
    margin-bottom: 60px;
    padding: 0 100px;

    &__box {
        width: 200px;
        height: 200px;
        background: #CCE9FE;
        border-radius: 50px;
        box-shadow: 0 15px 20px rgba(0, 0, 0, 0.3);
        cursor: pointer;
        transition: all 0.4s ease;
        &:hover{
            box-shadow: none;
        }
    }
}

.present-button{
display:flex;
justify-content: center;
& a{
    text-decoration: none;
}

.present-back{
    border: 1px solid #CCE9FE;
    text-align: center;
    padding: 10px 50px;
    color: #CCE9FE;
    font-size: 24px;
    border-radius: 20px;
    text-decoration: none;
    transition: all 0.4s ease;

    &:hover{
    background: #CCE9FE;
    color: #fff;
    box-shadow: 0 15px 20px rgba(0, 0, 0, 0.3);
    transition: 0.5 all ease;
    }
 }

}


`