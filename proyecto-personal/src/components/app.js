import React from 'react'
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from '../pages/home'
import PresentOne from '../pages/present-one'
import PresentTwo from '../pages/present-two'
import PresentThird from '../pages/present-third'
import NotFound from '../pages/404'

class App extends React.Component{

    render(){
        return(
     <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/berlin" component={PresentOne}/>
            <Route exact path="/sorpresa" component={PresentTwo}/>
            <Route exact path="/cena" component={PresentThird}/>
            <Route component={NotFound}/>
        </Switch>
     </BrowserRouter>
        )
    }

}

export default App
