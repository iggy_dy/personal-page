import React from 'react'
import { PresnetsComponentStyles } from './styles/presents-component-styles'


class PresentsComponent extends React.Component{
    render(){
        
        return (
          <PresnetsComponentStyles>
              
              <div className="presents-title">
                <p>Elige tu regalo</p>
              </div>
              <div className="presents-container">
               <a href="/sorpresa"><div className="presents-container__box"></div></a>
               <a href="/cena"><div className="presents-container__box"></div></a>
               <a href="/berlin"><div className="presents-container__box"></div></a>
              </div>

              <div className="present-button">
              <a href="/"><div className="present-back">
                Volver
              </div></a>
              </div>
          </PresnetsComponentStyles>
        );
    }
}

export default PresentsComponent