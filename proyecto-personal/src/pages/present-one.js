import React from "react";
import { PresnetStyles } from "./styles/present-styles";
import berlinCity from "../images/berlin.jpg";

class PresentOne extends React.Component {
  render() {
    return (
      <PresnetStyles>
        <div className="present-image">
          <img src={berlinCity} alt="" />
        </div>

        <div className="present-container">
          <div className="present-container-hero">
            <div className="present-titel">
              <p>Viaje a Berlín</p>
            </div>
            <div className="present-subtitle">
              Una ciudad llena de historia y lo mejor de todo: "to pagau"
            </div>
          </div>

          <div className="present-container-hero">
            <div className="present-titel left">
              <p>Un sitio para los dos</p>
            </div>
            <div className="present-subtitle left">
              Como a todos nos gusta viajar, y yo quiero que mi churry vea de
              todo, te regalo un vije a una ciudad desconocida para los dos, si
              bien sabemos que tenemos que ir a Dublin, Berlin es una
              posibilidad que espero que te haga ilu.
            </div>
          </div>

          <div className="present-container-hero">
            <div className="present-titel center">
              <p>¿Y en que fechas?</p>
            </div>
            <div className="present-subtitle center">
              31/1/2020 - 2/2/2020
            </div>
          </div>
        </div>
      </PresnetStyles>
    );
  }
}

export default PresentOne;
