import React from "react";
import { PresnetStyles } from "./styles/present-styles";

class NotFound extends React.Component {
  render() {
    return (
      <PresnetStyles>
        <div className="present-container">
        <div className="present-container-hero">
            <div className="present-titel center">
              <p>404... Pichurrada no encontrada :(</p>
            </div>
            <div className="present-subtitle center">
              Sigue buscando ^^
            </div>
          </div>
        </div>
      </PresnetStyles>
    );
  }
}

export default NotFound;
