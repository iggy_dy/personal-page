import styled from 'styled-components';

export const PresnetStyles = styled.div`
margin-bottom: 300px;


.present-image{
    background-color: #CCE9FE;
    filter:brightness(0.6);
    img{
        position: absolute;
        width:100%;
        clip-path: polygon(0 20%, 100% 40%, 100% 100%, 0 80%);
        z-index: -1;
        top: 100px;
    }
  } 


.present-container{
    padding: 100px 150px 0 150px;
}

.present-container-hero{
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    margin-bottom: 350px;

    .present-titel{
        text-align: right;
        color: #000;
        font-size: 36px;
        font-weight: 800;
        opacity: 0.3;
        letter-spacing: 10px;
        margin-bottom: 40px;

        &.left{
            text-align: left;
        }

        &.center{
            text-align: center;
        }
    }

    .present-subtitle{
        color: #000;
        font-size: 16px;
        font-weight: 500;
        opacity: 0.3;
        text-align: right;
        letter-spacing: 5px;
        line-height: 30px;
        margin: 0 0 300px 550px;

        &.left{
            text-align: left;
            margin: 0;
            max-width: 650px;
        }

        &.center{
            text-align: center;
            margin: 0;
        }
    }
}

.present-card-container{
    display: flex;
    justify-content: space-between;

    .present-card{
       padding: 50px 50px;
       border-radius: 30px;
       background: #fff;
       box-shadow: 0 15px 20px #000;
       width: 200px;
       transition: all 0.3s ease;
       cursor: pointer;

       &:hover {
           box-shadow: none;
       }
    }

    .present-card-title{
      color: #000;
      font-size: 24px;
      font-weight: 500;
      opacity: 0.3;
      letter-spacing: 2px;
      margin-bottom: 20px;
    }

    .present-card-text{
      color: #000;
      font-size: 14px;
      font-weight: 100;
      opacity: 0.3;
      letter-spacing: 2px;
    }
 }

`