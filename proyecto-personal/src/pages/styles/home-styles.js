import styled from 'styled-components';

export const HomeStyles = styled.div`

.home-image{
    img{
        position: absolute;
        width:100%;
        clip-path: polygon(0 50%, 100% 20%, 100% 70%, 0% 100%);
        z-index: -1;
    }
 }

 .home-container{
    padding: 100px 150px 0 150px;
 }
 .home-hero-title{
    color: #000;
    font-size: 36px;
    font-weight: 800;
    opacity: 0.3;
    letter-spacing: 10px;
    max-width: 650px;
    
     &__top{
        margin-bottom: 10px;
     }

     &__bottom{
        margin-bottom: 40px;
     }

     &__subtitle{
        font-size: 16px;
        letter-spacing: 5px;
        text-align: right;
        margin-bottom: 600px;
     }
 }

 .home-title{
    text-align: right;
    color: #000;
    font-size: 36px;
    font-weight: 800;
    opacity: 0.3;
    letter-spacing: 10px;
    margin-bottom: 40px;
    &.left{
        text-align: left;
    }

    &.bottom{
        text-align: center;
        font-size: 74px;
        font-weight: 500;
        letter-spacing: 40px;
        margin-bottom: 200px;
    }
 }

 .home-text{
    color: #000;
    font-size: 16px;
    font-weight: 500;
    opacity: 0.3;
    text-align: right;
    letter-spacing: 5px;
    line-height: 30px;
    margin: 0 0 200px 550px;
    &__content{
        margin-bottom: 20px;
    }
    &__img{
        img{
        opacity: 1;
        box-shadow: 0 15px 20px #CCE9FE;
        }
    }
    &.left{
      margin: 0 0 200px 0;
      text-align: left;
      max-width: 650px;
    }
 }

`;