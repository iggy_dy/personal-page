import React from 'react'
import { PresnetStyles } from "./styles/present-styles";
import dinnerTime from "../images/dinner-time.jpg";

class PresentThird extends React.Component{
    render(){
        return(
            <PresnetStyles>
            <div className="present-image">
              <img src={dinnerTime} alt="" />
            </div>
    
            <div className="present-container">
              <div className="present-container-hero">
                <div className="present-titel">
                  <p>Cena por el centro</p>
                </div>
                <div className="present-subtitle">
                  Una cena especial por tu día
                </div>
              </div>
    
              <div className="present-container-hero">
                <div className="present-titel left">
                  <p>¿Donde te apetece?</p>
                </div>
                <div className="present-subtitle left">
                  Al ser un día chulo y que es para tí, te invito a cenar
                  por el centro, proponiendote algunos sitios que seguro te gustarán.
                </div>
              </div>

              <div className="present-card-container">
                <div className="present-card">
                  <div className="present-card-title">
                   <p>Hamburguesa</p>
                  </div>
                  <div className="present-card-text">
                    <p>La mejor selección de hamurguesas en tribuna (lo conocí ayer...), ricas y contundentes
                      lo mejor para mi churry
                    </p>
                  </div>
                </div>

                <div className="present-card">
                  <div className="present-card-title">
                   <p>Sushi y wok</p>
                  </div>
                  <div className="present-card-text">
                    <p>Ya sabes donde es, que nos gusta mucho y esta muy rico, si ese,
                      el de Callao
                    </p>
                  </div>
                </div>

                <div className="present-card">
                  <div className="present-card-title">
                   <p>Donde quieras</p>
                  </div>
                  <div className="present-card-text">
                    <p>Ya sabes que soy muy poco imaginativo.... Elige algo antes
                      de que memontes un pollo
                    </p>
                  </div>
                </div>
              </div>
    
            </div>
          </PresnetStyles>
        )
    }
}

export default PresentThird