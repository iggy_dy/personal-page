import React from 'react'
import backgroundPresent from '../images/background-img.jpg'
import martaPhoto from '../images/marta.jpg'
import {HomeStyles} from './styles/home-styles'

class Home extends React.Component {
    render(){
        return (
          <HomeStyles>
              <div className="home-image">
              <img src={backgroundPresent} alt="" />
            </div>

            <div className="home-container">

            <div className="home-hero-title">
              <div className="home-hero-title__top">
                <p>Colección de Regalos</p>
              </div>
              <div className="home-hero-title__bottom">
                <p>para mi Churry</p>
              </div>
              <div className="home-hero-title__subtitle">
                <p>Porque se merece todo y más</p>
              </div>
            </div>

            <div className="home-title">
                <p>Introducción</p>
            </div>

            <div className="home-text">
            <div className="home-text__content">
                <p>Hoy es un día muy especial para mi churry, y por eso,
                    te he preparado esta sorpresa, sigue bajando y averigua como funciona
                    esta página.
                </p>
                </div>
                <div className="home-text__img">
                <img src={martaPhoto} alt=""/>
                </div>
            </div>

            <div className="home-title left">
                <p>Instrucciones</p>
            </div>

            <div className="home-text left">
                <p>En la parte de abajo de todas las páginas encontraras unas cajas,
                    esas cajas contienen regalos, que deberás abrir uno a uno y ver lo que
                    hay dentro.
                </p>
            </div>

            </div>            
          </HomeStyles>
        );
    }
}

export default Home