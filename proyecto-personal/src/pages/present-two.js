import React from 'react'
import { PresnetStyles } from "./styles/present-styles";

class PresentTwo extends React.Component{
    render(){
        return(
        <PresnetStyles>
        <div className="present-container">
        <div className="present-container-hero">
            <div className="present-titel center">
              <p>Mira debajo de la cama</p>
            </div>
            <div className="present-subtitle center">
              Hay sorpresa ^^
            </div>
          </div>
        </div>
        </PresnetStyles>
        )
    }
}

export default PresentTwo