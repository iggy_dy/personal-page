// const element = document.createElement('h1')

// element.innerText='Hola Mundo'

// const container = document.getElementById ('root')

// container.appendChild (element)

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from './App-styles'
import App from './components/app'
import 'reset-css';
import PresentsComponent from './components/presents-component'

const container = document.getElementById(`root`)
const content = (
<AppContainer>
<App/>
<PresentsComponent/>
</AppContainer>

    
)




ReactDOM.render(content, container)