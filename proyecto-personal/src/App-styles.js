import styled from 'styled-components';

export const AppContainer = styled.div`
	@import url('https://fonts.googleapis.com/css?family=Alata&display=swap');

	font-family: 'Alata', sans-serif;
`;